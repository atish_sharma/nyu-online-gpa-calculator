var UtilityModule=require('./UtilityModule.js').UtilityModule;

var TAG='EmailModule';

exports.EmailModule={

	transporter:null,

	init:function(transporter){
		UtilityModule.log(TAG,'init');
		this.transporter=transporter;
	},
	
	emailResult:function(result,recipients,next){
		UtilityModule.log(TAG,'emailResult');
		try{
			var htmlFormattedMessage='';
			htmlFormattedMessage+='<center>';
			htmlFormattedMessage+='<h3>Semester blueprint :</h3>';
			htmlFormattedMessage+='<table border="1px">';
			htmlFormattedMessage+='<tr>';
			htmlFormattedMessage+='<th>Name</th>';
			htmlFormattedMessage+='<th>Credits</th>';
			htmlFormattedMessage+='<th>Grade</th>';
			htmlFormattedMessage+='</tr>';
			for(var i=0;i<result.subjects.length;i++){
				var subject=result.subjects[i];
				htmlFormattedMessage+='<tr>';
				htmlFormattedMessage+='<td>'+subject.name+'</td>';
				htmlFormattedMessage+='<td>'+subject.credits+'</td>';
				htmlFormattedMessage+='<td>'+subject.grade+'</td>';
				htmlFormattedMessage+='</tr>';
			}
			htmlFormattedMessage+='</table>';
			htmlFormattedMessage+='</center>';
			htmlFormattedMessage+='<p>GPA : <b style="color:#ff0000">'+result.gpa+'</b></p>';
			htmlFormattedMessage+='<br/>';
			htmlFormattedMessage+='<a href="http://www.atishsharma.online:2000/">Nyu - online GPA calculator</a>';
			var mailOptions={
				to:recipients,
				subject:'Nyu - online GPA calculator',
				html:htmlFormattedMessage
			};
			this.transporter.sendMail(mailOptions,next);
		}catch(err){
			next(err,null);
		}
	}

};