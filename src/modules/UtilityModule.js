require('../../node_modules/node-env-file')('env');
var sanitize=require('../../node_modules/mongo-sanitize');
var _=require('../../node_modules/underscore');

var TAG='UtilityModule';

exports.UtilityModule={

	log:function(tag,data,json){
		if(process.env.LOG=='true'){
			console.log(tag,data);
			if(!_.isUndefined(json) && !_.isNull(json)){
				console.log('........................................');
				console.log(JSON.stringify(json,null,2));
			}
			console.log('----------------------------------------');
		}
	},

	sanitize:function(str){
		this.log(TAG,'sanitize');
		if(this.isUsableString(str)){
			return sanitize(str.trim());
		}
		return null;
	},

	isUsable:function(obj){
		this.log(TAG,'isUsable');
		return !_.isUndefined(obj) && !_.isNull(obj);
	},

	isUsableString:function(str){
		this.log(TAG,'isUsableString');
		return this.isUsable(str) && _.isString(str);
	}

};