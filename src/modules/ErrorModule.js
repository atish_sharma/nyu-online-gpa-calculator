exports.ErrorModule={
	INVALID_RECIPIENT_LIST:'Invalid recipient (list) provided.',
	INVALID_RESULT:'Invalid result provided.',
	INVALID_RESULT_AND_RECIPIENT_LIST:'Invalid result and recipient (list) provided.',
	UNKNOWN_ERROR:'An unknown error occured! Please check the email(s) provided.'
};