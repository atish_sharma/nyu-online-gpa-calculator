var UtilityModule=require('../modules/UtilityModule.js').UtilityModule;
var ConnectionModule=require('../modules/ConnectionModule.js').ConnectionModule;
var EmailService=require('../services/EmailService.js').EmailService;

var TAG='ConnectionController';

exports.ConnectionController=function(io){

	var self=this;
	self.io=io;
	self.socket=null;
	self.emailService=null;

	self.connect=function(socket){
		UtilityModule.log(TAG,'connect');
		self.socket=socket;
		UtilityModule.log(TAG,self.socket.id+' connected');
		self.instantiateServices();
		self.setListeners();
	};

	self.instantiateServices=function(){
		UtilityModule.log(TAG,'instantiateServices');
		self.emailService=new EmailService(self.socket);
	};

	self.setListeners=function(){
		UtilityModule.log(TAG,'setListeners');
		self.socket.on(ConnectionModule.EMAIL,self.emailService.sendEmail);
	};

	self.disconnect=function(){
		UtilityModule.log(TAG,'disconnect');
		UtilityModule.log(TAG,self.socket.id+' disconnected');
	};

};