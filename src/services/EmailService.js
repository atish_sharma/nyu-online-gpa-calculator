var UtilityModule=require('../modules/UtilityModule.js').UtilityModule;
var ApiModule=require('../modules/ApiModule.js').ApiModule;
var ConnectionModule=require('../modules/ConnectionModule.js').ConnectionModule;
var ErrorModule=require('../modules/ErrorModule.js').ErrorModule;
var EmailModule=require('../modules/EmailModule.js').EmailModule;

var TAG='EmailService';

exports.EmailService=function(socket){

	var self=this;
	self.socket=socket;

	self.sendEmail=function(data){
		UtilityModule.log(TAG,'sendEmail');
		data[ApiModule.RECIPIENTS]=UtilityModule.sanitize(data[ApiModule.RECIPIENTS]);
		var emailResult=true;
		var email={};
		if(!UtilityModule.isUsable(data[ApiModule.RESULT])){
			emailResult=false;
			email[ApiModule.ERROR]=ErrorModule.INVALID_RESULT;
		}
		if(!UtilityModule.isUsableString(data[ApiModule.RECIPIENTS]) || data[ApiModule.RECIPIENTS]==''){
			if(emailResult){
				emailResult=false;
				email[ApiModule.ERROR]=ErrorModule.INVALID_RECIPIENT_LIST;
			}else{
				email[ApiModule.ERROR]=ErrorModule.INVALID_RESULT_AND_RECIPIENT_LIST;
			}
		}
		if(emailResult){
			EmailModule.emailResult(data[ApiModule.RESULT],data[ApiModule.RECIPIENTS],function(err,res){
				if(err==null){
					email[ApiModule.SUCCESS]=true;
				}else{
					UtilityModule.log(TAG,err);
					email[ApiModule.SUCCESS]=false;
					email[ApiModule.ERROR]=ErrorModule.UNKNOWN_ERROR;
				}
				self.socket.emit(ConnectionModule.EMAIL,email);
			});
		}else{
			email[ApiModule.SUCCESS]=false;
			self.socket.emit(ConnectionModule.EMAIL,email);
		}	
	};

};