# Nyu #

# Purpose #

Nyu is a web application which allows its users to calculate their GPA. It takes course information (such as course credits and course name) and it's corresponding grade (this grade can be a mere prediction) as user input. GPA is calculated on a 10 point scale. The user is also provided with the option of having the result emailed to him/her. This is useful if the user wants to store or share the result for a particular input.   

# Project Scope #

The system focuses on only one class of user, interacting with the system directly. Through the system, the various users will be able to calculate their GPA according to the provided input. This enables the user to easily predict the GPA that he/she might get for a given semester.

# Operating Environment #

1. Latest web browser
2. Working internet connection