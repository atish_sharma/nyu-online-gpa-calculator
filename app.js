//requiring node modules and instantiating essential objects
require('node-env-file')('env');
var express=require('express');
var app=express();
var http=require('http').Server(app);
var io=require('socket.io')(http);
var nodemailer=require('nodemailer');

//requiring routes
require(__dirname+'/src/routes/IndexRouter.js')(app);

//configuring app to use static files
app.use('/static',express.static(__dirname+'/public'));

//requiring classes from other files
var ConnectionController=require(__dirname+'/src/controllers/ConnectionController.js').ConnectionController;
var UtilityModule=require(__dirname+'/src/modules/UtilityModule.js').UtilityModule;
var EmailModule=require(__dirname+'/src/modules/EmailModule.js').EmailModule;
var ConnectionModule=require(__dirname+'/src/modules/ConnectionModule.js').ConnectionModule;

//setting variables
var PORT=process.env.PORT;
var TAG='app';
app.locals.dir=__dirname;
var transporter=nodemailer.createTransport(process.env.EMAIL_PROTOCOL+'://'+process.env.EMAIL_PREFIX+'%40'+process.env.EMAIL_SUFFIX+':'+process.env.EMAIL_PASSWORD+'@'+process.env.EMAIL_SERVICE);
EmailModule.init(transporter);
var connectionController=new ConnectionController(io);

//listening on a port
http.listen(PORT,function(){
  UtilityModule.log(TAG,'Listening on port '+PORT);
});

//listening for a connection
io.on(ConnectionModule.CONNECT,connectionController.connect);