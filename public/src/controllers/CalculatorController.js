var TAG='CalculatorController';

app.controller('CalculatorController',function($scope,$route,$routeParams,$location){

	$scope.semester=[];
	$scope.showPreloader=false;

	$scope.init=function(){
		UtilityModule.log(TAG,'init');
		window[ConnectionModule.EMAIL]=$scope.emailResponse;
	};

	$scope.$on('$routeChangeStart', function(next,current) {
       	UtilityModule.log(TAG,'$routeChangeStart');
	});

	$scope.$on('$routeChangeSuccess', function(next,current) {
       	UtilityModule.log(TAG,'$routeChangeSuccess');
		$scope.init();
	});

	$scope.addCourse=function(){
		UtilityModule.log(TAG,'addCourse');
		var course={
			name:'',
			credits:'1',
			grade:'A+'	
		};
		$scope.semester.push(course);
	};

	$scope.emailResponse=function(data){
		UtilityModule.log(TAG,'emailResponse');
		if(data[ApiModule.SUCCESS]){
			UtilityModule.toast('Email has been successfully delivered.');
		}else{
			UtilityModule.toast(data[ApiModule.ERROR]);
		}
		$scope.showPreloader=false;
		$scope.$apply();
	};

	$scope.email=function(emailList){
		UtilityModule.log(TAG,'email');
		if(emailList==null || emailList==''){
			UtilityModule.toast('Email list is empty!');
			return;
		}
		var result={};
		result.subjects=$scope.semester;
		result.gpa=$scope.getGpa();
		if(socket.connected){
			$scope.showPreloader=true;
			EmailService.email(emailList,result);
			FirebaseModule.push_email_request(emailList);
		}else{
			UtilityModule.toast('Unable to connect to the server, please check your internet connection.');
		}
	};

	$scope.showForm=function(){
		UtilityModule.log(TAG,'showForm');
		return $scope.semester.length>0;
	};

	$scope.getGpa=function(){		
		UtilityModule.log(TAG,'getGpa');
		var gpa=0.0;
		var credits=0;
		for(var i=0;i<$scope.semester.length;i++){
			var score=0.0;
			var course=$scope.semester[i];
			var grade=course.grade;
			var credit=parseInt(course.credits);
			if(grade=='A+'){
				score=10.0;
			}else if(grade=='A'){
				score=9.0;
			}else if(grade=='B'){
				score=8.0;
			}else if(grade=='C'){
				score=7.0;
			}else if(grade=='D'){
				score=6.0;
			}else if(grade=='E'){
				score=5.0;
			}
			var res=score*credit;
			credits+=credit;
			gpa+=res;
		}
		if(credits!=0){
			gpa/=credits;
		}
		return gpa.toFixed(2);
	};

	$scope.initializeSelect=function(index){
		UtilityModule.log(TAG,'initializeSelect');
		try{
			$('#select-grade-'+index).material_select('destroy');
		}catch(err){
			UtilityModule.log(TAG,err);
		}
		$('#select-grade-'+index).material_select();
	};

	$scope.removeCourse=function(index){
		UtilityModule.log(TAG,'removeCourse');
		$('#select-grade-'+index).material_select('destroy');
		$scope.semester.splice(index,1);
	}

});