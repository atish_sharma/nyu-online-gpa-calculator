app.config(function($routeProvider){
	$routeProvider
    .when('/', {
    	templateUrl:'static/views/calculator.html',
        controller:'CalculatorController'
    })
    .otherwise({
    	redirectTo:'/'
    });
});