app.directive('header',function(){
	return{
		restrict:'E',
		link:function(scope,element,attributes){
			scope.title=attributes['title'];
		},
		template:function(element,attributes){
			var t='';
			t+='<div class="header color-text">';
			t+='{{title}}';
			t+='</div>';
			return t;
		}
	};
});