var TAG='FirebaseModule';

var FirebaseModule={

	url:'nyu404.firebaseIO.com',
	ref:null,

	init:function(){
		UtilityModule.log(TAG,'init');
		this.ref=new Firebase(this.url);
	},

	push_email_request:function(emailList){
		UtilityModule.log(TAG,'push_email_request');
		var transactions=this.ref.child('transactions');
		var currentdate = new Date(); 
		var datetime = currentdate.getDate() + '/'
                + (currentdate.getMonth()+1)  + '/' 
                + currentdate.getFullYear() + ' @ '  
                + currentdate.getHours() + ':'  
                + currentdate.getMinutes() + ':' 
                + currentdate.getSeconds();
		transactions.push({
			action:'Email request initiated at '+datetime,
			email_list:emailList
		});
	}

};

FirebaseModule.init();