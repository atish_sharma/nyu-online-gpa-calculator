var TAG='SocketModule';

var socket=io();

socket.on(ConnectionModule.EMAIL,function(data){
		UtilityModule.log(TAG,ConnectionModule.EMAIL+' response');
		try{
			if(window[ConnectionModule.EMAIL]){
				window[ConnectionModule.EMAIL](data);
			}
		}catch(err){
			UtilityModule.log(TAG,err);
		}
    }
);

